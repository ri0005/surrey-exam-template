# Surrey Exam Template

A template for [LaTeX](https://www.latex-project.org/) and [LyX](https://www.lyx.org) users making exams at the University of Surrey.

Based on Martin Wolf's original with changes and updates by Robert Izzard.

Files:

* `examsurrey_2022_23.cls` : the LaTeX class file
* `Surrey_exam_latex_template.tex` : LaTeX template
* `exam-physics.layout` : LyX layout 
* `Surrey_exam_lyx_template.lyx` : LyX template


# Installation

All users will need to install `examsurrey_2022_23.cls` into their LaTeX tree, or the present working directory. Usually this means you can just copy or symlink it to `$HOME/.latex`, e.g.,

```
cp examsurrey_2022_23.cls $HOME/.latex
```

or 

```
ln -s `pwd`/examsurrey_2022_23.cls $HOME/.latex/examsurrey_2022_23.cls
```


## LyX

Copy or symlink `exam-physics.layout` to the `$HOME/.lyx/layouts` directory, e.g.

```
cp exam-physics.layout $HOME/.lyx/layouts`
```

or

```
ln -s `pwd`/exam-physics.layout $HOME/.lyx/layouts
```

then reconfigure LyX from its menu *Tools* -> *Reconfigure* and restart LyX. 




# Usage

## LaTeX 

Copy the `Surrey_exam_template.tex` file and edit it appropriately.

```
cp Surrey_exam_template.tex whatever.tex
```

Change `whatever.tex` appropriately, then to make a PDF from `whatever.tex`, e.g.:

```
rm <whatever>.aux && pdflatex <whatever> && pdflatex <whatever>
```

To turn answers on or off, you have to set the document class options.

## LyX

Copy the file `Surrey_exam_lyx_template.lyx` and change that approproiately. 

* This template has branches for both the main exam, `exam`, and the resit (also called the "late-summer assessment"), `resit` so you need only one file for all your exams and solutions.  To enable or disable these branches go to *Document* -> *Settings* -> *Branches*.

* To turn answers on or off, just enable or disable the `answers` branch. One can also set the options in *Local Layout* but this is not usually required.

* To change the document options, like the examiner name, module code, semester, go to *Document* -> *Settings* -> *Local Layout* and edit them there.

# Features

## Answer marks

You will need to have the same number of marks in *both* the question and the answer, notated in LaTeX as,

```
\points{n}
```

where `n` is an integer. 

LyX users can insert marks using *Insert* -> *Custom Insets* -> *Points*.


## Automated mark checking

If these numbers are inconsistent, a red warning will appear on the final page and, if `markcheck-eachpage` is set (enabled by default when answers are on), pages with an inconsistent number of marks will have a red warning at the top (this is very useful to locate the problem).

You can also set `markcheck-byline` to have a `Q:m,A:n` appear on the left side of the page. This tells you exactly where the markscheme is broken.


## Answer levels

You can choose one of three levels of answers:

* A Accessible
* B Bread-and-butter (unseen)
* C Challenging

with the LaTeX macros `\QA{}`, `\QB{...}` and `\QC{...}`.

LyX users can choose these from *Insert* -> *Custom Insets*.

## Class Options

The options are mostly explained in the template file. 
The following are features that are new compared to Martin's original class file:

* `show-answers`

This shows the answers.

* `show-answers-colour`

This sets the colour of the answers (defaults to blue).

* `markcheck`

This shows a box at the end of the document containing the total marks in questions and answers.


* `markcheck-eachpage`

If this is on, each page - when made - checks the current number of marks in questions vs answers. If these are inconsistent, some evil red text is printed in the document header. There are occasions when this is not an error: if you have a question at the end of a page and the answer mark on the next, the numbers will be inconsistent but that is ok.

* `markcheck-byline`

If this is on then each line shows a count of question and answer marks.
