#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass exam-physics
\begin_preamble

 
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\begin_local_layout

ClassOptions
    # Customize the Surrey exam class here
    #
    # Packages we (might) require
    Other "custom-packages={amsfonts,amsmath,amssymb,bbm,bm,graphicx,mathrsfs,tikz}"

    # portrait, or comment out for landscape
    Other "portrait"
    
    # Font size
    Other "fontsize-n" # normal size font

    #Other "fontsize-m" # medium size font
    #Other "fontsize-l" # large size font
    #Other "fontsize-xl" # extra large size font
    #Other "fontsize-xxl" # super large size font

    # Type of paper
    Other "exam" # for examination
    #Other "coursework" # for coursework
    #Other "test" # for in-semester test

    # Submission type
    #Other "submit-online" # comment out for standard offline submission, when online due date and time are required

    # Level
    #Other "ug-1" # for modules in year 1
    #Other "ug-2" # for modules in year 2
    Other "ug-3" # for modules in year 3
    #Other "ug-4" # for modules in year 4 and shared between year 4 and MSc 
    #Other "pg" # for MSc modules

    # Academic year in format YY
    Other "academic-year={22}"

    # Additional material
    #Other "additional-material={Graph Paper, Formula Booklet}"    
    Other "additional-material={Physics Department Booklet of Formulae and Data,\\ One side of A4 paper of handwriting and/or typeset notes}"

    # Calculators
    Other "allow-calculators" # comment out if not allowed

    # Credits
    Other "credits={15}"

    # Department
    Other "department-physics"
    #Other "department-mathematics"

    # Due date; for courseworks and online submissions
    #    Other "due-date={2021-06-30}" #format is YYYY-MM-DD
    #    Other "due-time={16:00}" #format is HH:MM

    # External examiner
    Other "external-examiner={Prof T Lancaster}"

    # Number of handouts; only used for the following departments: Mathematics
    Other "handouts={0}"

    # Has parts (i.e. Part A, Part B, etc)
    Other "has-parts"

    # Internal examiner (module convenor)
    Other "internal-examiner={Dr M Kermode}"

    # Module code
    Other "module-code={PHY1234}"

    # Module title
    Other "module-title={A Study of Things}"

    # Semester; choose 0 for late summer assessment
    Other "semester={1}"

    # Show URN field for students to leave their URN on front page
    Other "show-urn"

    # Weighting between 0 and 100; proportion of the total module mark
    Other "weighting={70}"

    # show answers? always set to yes: we control in LyX
    # with the branches
    Other "show-answers"

    # colour of answers
    Other "show-answers-colour={blue}"

    # check marks on each page 
    Other "markcheck-eachpage"

    # check marks at each line
    Other "markcheck-eachline"

    # show marks at end of document
    Other "markcheck"
End

# enforce semester==0 for LSA/resit
InsetLayout Branch:resit
Preamble 
\renewcommand\semester{0}
EndPreamble
End
\end_local_layout
\begin_forced_local_layout
Format 66
Style "Question"
	Category "MainText"
	Margin Static
	LatexType Environment
	InTitle 0
	InPreamble 0
	TocLevel -1000
	ResumeCounter 0
	StepMasterCounter 0
	NeedProtect 0
	KeepEmpty 0
	LabelFont
		Series bold
	EndFont
	NextNoIndent 0
	CommandDepth 0
	LatexName "question"
	ItemCommand item
	LabelType Centered
	EndLabelType No_Label
	ParagraphGroup "0"
	ParSkip 0
	ItemSep 1
	TopSep 0
	BottomSep 0
	LabelBottomSep 0
	LabelSep x
	ParSep 0
	NewLine 1
	Align Block
	AlignPossible Layout
	LabelString "Question \Alph{exampartcounter}.\arabic{questioncounter}"
	LabelCounter "questioncounter"
	FreeSpacing 0
	PassThru 0
	ParbreakIsNewline 0
	RefPrefix OFF
	HTMLLabelFirst 0
	HTMLForceCSS 0
	HTMLTitle 0
	Spellcheck 1
	ForceLocal 1
End
Style "Exam-Part"
	Category "MainText"
	Margin Dynamic
	LatexType Command
	InTitle 0
	InPreamble 0
	TocLevel 1
	ResumeCounter 0
	StepMasterCounter 0
	Argument 1
		LabelString "Short Title|S"
		InsertCotext 1
		ToolTip "The section as it appears in the table of contents/running headers"
		PassThru inherited
	EndArgument
	NeedProtect 1
	KeepEmpty 1
	Font
		Series bold
		Size larger
	EndFont
	NextNoIndent 1
	CommandDepth 0
	LatexName "exampart"
	ItemCommand item
	LabelType Static
	EndLabelType No_Label
	ParagraphGroup "0"
	ParSkip 0.4
	ItemSep 0
	TopSep 1.3
	BottomSep 0.7
	LabelBottomSep 0
	LabelSep x
	ParSep 0.7
	NewLine 1
	Align Block
	AlignPossible Layout
	LabelString "Exam Part: \Alph{exampartcounter}"
	LabelCounter "exampartcounter"
	FreeSpacing 0
	PassThru 0
	ParbreakIsNewline 0
	RefPrefix sec
	HTMLTag h2
	HTMLLabelFirst 0
	HTMLForceCSS 0
	HTMLTitle 0
	Spellcheck 1
	ForceLocal 1
End
\end_forced_local_layout
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family sfdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type authoryear
\biblio_style plainnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\branch answers
\selected 1
\filename_suffix 0
\color #faf0e6
\end_branch
\branch exam
\selected 1
\filename_suffix 0
\color #faf0e6
\end_branch
\branch resit
\selected 0
\filename_suffix 0
\color #faf0e6
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\topmargin 3cm
\rightmargin 2cm
\bottommargin 3cm
\secnumdepth 1
\tocdepth 1
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Branch answers
inverted 1
status open

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
leave this here: it is run when the answers branch is disabled
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\backslash
notanswers{
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Exam-Part

\end_layout

\begin_layout Question
\begin_inset Branch exam
inverted 0
status collapsed

\begin_layout Standard
First short question
\begin_inset Flex Points
status open

\begin_layout Plain Layout
5
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status open

\begin_layout Standard
\begin_inset Flex QA-Accessible-bookwork-core
status open

\begin_layout Itemize
Answers
\begin_inset Flex Points
status open

\begin_layout Plain Layout
5
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Branch resit
inverted 0
status collapsed

\begin_layout Standard
First resit question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
7
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Itemize
\begin_inset Flex QA-Accessible-bookwork-core
status open

\begin_layout Plain Layout
Resit answers.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
4
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Flex QB-Bread-and-butter-unseen
status open

\begin_layout Plain Layout
More resit answers.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Question
\begin_inset Branch exam
inverted 0
status collapsed

\begin_layout Standard
Second A question.
 
\end_layout

\begin_layout Enumerate
First part
\end_layout

\begin_layout Enumerate
Second part
\end_layout

\begin_layout Enumerate
Third part
\end_layout

\begin_layout Standard
\begin_inset Flex Points
status open

\begin_layout Plain Layout
5
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status open

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Itemize
First answer.
 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Second answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Third answer.
 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Branch resit
inverted 0
status collapsed

\begin_layout Standard
Second A resit question.
 
\end_layout

\begin_layout Enumerate
First part
\end_layout

\begin_layout Enumerate
Second part
\end_layout

\begin_layout Enumerate
Third part
\end_layout

\begin_layout Standard
\begin_inset Flex Points
status open

\begin_layout Plain Layout
5
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status open

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Itemize
First answer.
 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Second answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Third answer.
 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Question
\begin_inset Branch exam
inverted 0
status collapsed

\begin_layout Standard
Third A question.
\end_layout

\begin_layout Enumerate
First part
\end_layout

\begin_layout Enumerate
Second part
\end_layout

\begin_layout Enumerate
Third part
\end_layout

\begin_layout Standard
\begin_inset Flex Points
status open

\begin_layout Plain Layout
5
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status open

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Itemize
First answer.
 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Second answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Third answer.
 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Branch resit
inverted 0
status collapsed

\begin_layout Standard
Third A resit question.
\end_layout

\begin_layout Enumerate
First part
\end_layout

\begin_layout Enumerate
Second part
\end_layout

\begin_layout Enumerate
Third part
\end_layout

\begin_layout Standard
\begin_inset Flex Points
status open

\begin_layout Plain Layout
5
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Itemize
First answer.
 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Second answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Third answer.
 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Exam-Part

\end_layout

\begin_layout Question
\begin_inset Branch exam
inverted 0
status collapsed

\begin_layout Standard
A longer type B question.
\end_layout

\begin_layout Enumerate
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Explain the meaning of the symbols in the stellar-structure equations.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
4
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Itemize
\begin_inset Formula $r$
\end_inset

 is the radial coordinate, i.e.
 distance from the centre of the star [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $m\left(r\right)$
\end_inset

 is the mass interior to 
\begin_inset Formula $r$
\end_inset

 (not mass 
\emph on
at
\emph default
 
\begin_inset Formula $r$
\end_inset

 which is meaningless) [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $P\left(r\right)$
\end_inset

 is the pressure at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $L\left(r\right)$
\end_inset

 is the luminosity generated by mass interior to 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $T\left(r\right)$
\end_inset

 is the temperature at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\rho\left(r\right)$
\end_inset

 is the density at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\kappa\left(r\right)$
\end_inset

 is the opacity at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\sigma$
\end_inset

 is the Stefan-Boltzmann constant and 
\begin_inset Formula $G$
\end_inset

 is the gravitational constant [1/2]
\end_layout

\begin_layout Itemize
No marks for explaining what the numbers or 
\begin_inset Formula $\pi$
\end_inset

 mean, we're neither that trivial nor that cruel.
\end_layout

\end_inset


\end_layout

\end_inset

A question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QA-Accessible-bookwork-core
status open

\begin_layout Plain Layout
Answer 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset

 and more answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Enumerate
Another question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Plain Layout
Very long answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Branch resit
inverted 0
status collapsed

\begin_layout Standard
A longer type B resit question.
\end_layout

\begin_layout Enumerate
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Explain the meaning of the symbols in the stellar-structure equations.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
4
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Itemize
\begin_inset Formula $r$
\end_inset

 is the radial coordinate, i.e.
 distance from the centre of the star [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $m\left(r\right)$
\end_inset

 is the mass interior to 
\begin_inset Formula $r$
\end_inset

 (not mass 
\emph on
at
\emph default
 
\begin_inset Formula $r$
\end_inset

 which is meaningless) [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $P\left(r\right)$
\end_inset

 is the pressure at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $L\left(r\right)$
\end_inset

 is the luminosity generated by mass interior to 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $T\left(r\right)$
\end_inset

 is the temperature at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\rho\left(r\right)$
\end_inset

 is the density at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\kappa\left(r\right)$
\end_inset

 is the opacity at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\sigma$
\end_inset

 is the Stefan-Boltzmann constant and 
\begin_inset Formula $G$
\end_inset

 is the gravitational constant [1/2]
\end_layout

\begin_layout Itemize
No marks for explaining what the numbers or 
\begin_inset Formula $\pi$
\end_inset

 mean, we're neither that trivial nor that cruel.
\end_layout

\end_inset


\end_layout

\end_inset

A question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
5
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status open

\begin_layout Standard
\begin_inset Flex QA-Accessible-bookwork-core
status open

\begin_layout Plain Layout
Answer 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset

 and more answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Enumerate
Another question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
15
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status open

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Plain Layout
Very long answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
15
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Question
\begin_inset Branch exam
inverted 0
status collapsed

\begin_layout Standard
A longer type B question.
\end_layout

\begin_layout Enumerate
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Explain the meaning of the symbols in the stellar-structure equations.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
4
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Itemize
\begin_inset Formula $r$
\end_inset

 is the radial coordinate, i.e.
 distance from the centre of the star [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $m\left(r\right)$
\end_inset

 is the mass interior to 
\begin_inset Formula $r$
\end_inset

 (not mass 
\emph on
at
\emph default
 
\begin_inset Formula $r$
\end_inset

 which is meaningless) [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $P\left(r\right)$
\end_inset

 is the pressure at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $L\left(r\right)$
\end_inset

 is the luminosity generated by mass interior to 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $T\left(r\right)$
\end_inset

 is the temperature at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\rho\left(r\right)$
\end_inset

 is the density at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\kappa\left(r\right)$
\end_inset

 is the opacity at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\sigma$
\end_inset

 is the Stefan-Boltzmann constant and 
\begin_inset Formula $G$
\end_inset

 is the gravitational constant [1/2]
\end_layout

\begin_layout Itemize
No marks for explaining what the numbers or 
\begin_inset Formula $\pi$
\end_inset

 mean, we're neither that trivial nor that cruel.
\end_layout

\end_inset


\end_layout

\end_inset

A question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QA-Accessible-bookwork-core
status open

\begin_layout Plain Layout
Answer 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset

 and more answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Enumerate
Another question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Plain Layout
Very long answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Branch resit
inverted 0
status collapsed

\begin_layout Standard
A longer type B resit question.
\end_layout

\begin_layout Enumerate
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Explain the meaning of the symbols in the stellar-structure equations.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
4
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Itemize
\begin_inset Formula $r$
\end_inset

 is the radial coordinate, i.e.
 distance from the centre of the star [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $m\left(r\right)$
\end_inset

 is the mass interior to 
\begin_inset Formula $r$
\end_inset

 (not mass 
\emph on
at
\emph default
 
\begin_inset Formula $r$
\end_inset

 which is meaningless) [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $P\left(r\right)$
\end_inset

 is the pressure at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $L\left(r\right)$
\end_inset

 is the luminosity generated by mass interior to 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $T\left(r\right)$
\end_inset

 is the temperature at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\rho\left(r\right)$
\end_inset

 is the density at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\kappa\left(r\right)$
\end_inset

 is the opacity at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\sigma$
\end_inset

 is the Stefan-Boltzmann constant and 
\begin_inset Formula $G$
\end_inset

 is the gravitational constant [1/2]
\end_layout

\begin_layout Itemize
No marks for explaining what the numbers or 
\begin_inset Formula $\pi$
\end_inset

 mean, we're neither that trivial nor that cruel.
\end_layout

\end_inset


\end_layout

\end_inset

A question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QA-Accessible-bookwork-core
status open

\begin_layout Plain Layout
Answer 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset

 and more answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Enumerate
Another question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Plain Layout
Very long answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Question
\begin_inset Branch exam
inverted 0
status collapsed

\begin_layout Standard
A longer type B question.
\end_layout

\begin_layout Enumerate
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Explain the meaning of the symbols in the stellar-structure equations.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
4
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Itemize
\begin_inset Formula $r$
\end_inset

 is the radial coordinate, i.e.
 distance from the centre of the star [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $m\left(r\right)$
\end_inset

 is the mass interior to 
\begin_inset Formula $r$
\end_inset

 (not mass 
\emph on
at
\emph default
 
\begin_inset Formula $r$
\end_inset

 which is meaningless) [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $P\left(r\right)$
\end_inset

 is the pressure at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $L\left(r\right)$
\end_inset

 is the luminosity generated by mass interior to 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $T\left(r\right)$
\end_inset

 is the temperature at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\rho\left(r\right)$
\end_inset

 is the density at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\kappa\left(r\right)$
\end_inset

 is the opacity at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\sigma$
\end_inset

 is the Stefan-Boltzmann constant and 
\begin_inset Formula $G$
\end_inset

 is the gravitational constant [1/2]
\end_layout

\begin_layout Itemize
No marks for explaining what the numbers or 
\begin_inset Formula $\pi$
\end_inset

 mean, we're neither that trivial nor that cruel.
\end_layout

\end_inset


\end_layout

\end_inset

A question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QA-Accessible-bookwork-core
status open

\begin_layout Plain Layout
Answer 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset

 and more answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Enumerate
Another question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Plain Layout
Very long answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Branch resit
inverted 0
status collapsed

\begin_layout Standard
A longer type B resit question.
\end_layout

\begin_layout Enumerate
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Explain the meaning of the symbols in the stellar-structure equations.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
4
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Itemize
\begin_inset Formula $r$
\end_inset

 is the radial coordinate, i.e.
 distance from the centre of the star [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $m\left(r\right)$
\end_inset

 is the mass interior to 
\begin_inset Formula $r$
\end_inset

 (not mass 
\emph on
at
\emph default
 
\begin_inset Formula $r$
\end_inset

 which is meaningless) [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $P\left(r\right)$
\end_inset

 is the pressure at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $L\left(r\right)$
\end_inset

 is the luminosity generated by mass interior to 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $T\left(r\right)$
\end_inset

 is the temperature at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\rho\left(r\right)$
\end_inset

 is the density at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\kappa\left(r\right)$
\end_inset

 is the opacity at 
\begin_inset Formula $r$
\end_inset

 [1/2]
\end_layout

\begin_layout Itemize
\begin_inset Formula $\sigma$
\end_inset

 is the Stefan-Boltzmann constant and 
\begin_inset Formula $G$
\end_inset

 is the gravitational constant [1/2]
\end_layout

\begin_layout Itemize
No marks for explaining what the numbers or 
\begin_inset Formula $\pi$
\end_inset

 mean, we're neither that trivial nor that cruel.
\end_layout

\end_inset


\end_layout

\end_inset

A question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QA-Accessible-bookwork-core
status open

\begin_layout Plain Layout
Answer 
\begin_inset Flex Points
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset

 and more answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Enumerate
Another question.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\begin_inset Branch answers
inverted 0
status collapsed

\begin_layout Standard
\begin_inset Flex QC-Challenging
status open

\begin_layout Plain Layout
Very long answer.
\begin_inset Flex Points
status open

\begin_layout Plain Layout
17
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
