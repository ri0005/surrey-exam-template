% Version 2.0
% Last update: 12th March 2023
% Contact: Robert Izzard (r.izzard@surrey.ac.uk)
% Original by: Martin Wolf (m.wolf@surrey.ac.uk)
% Requirements: TeX Live 2019+

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{examsurrey_2022_23}

%
% Load class
%

\LoadClass[
	a4paper,
	12pt,
	twoside
]{article}

%
% Packages for declaring options
%

\RequirePackage{etoolbox}
%\RequirePackage{kvoptions-patch} % No longer works in TeXLive 2020
\RequirePackage{xintexpr}
\RequirePackage{xkeyval}
\RequirePackage{xkvltxp} % This replaces the kvoptions-patch; see above

%
% Options
%

% Submission type
\newif\if@online\@onlinefalse
\DeclareOptionX{submit-online}{\@onlinetrue}

% Layout
\newif\if@portrait\@portraitfalse
\DeclareOptionX{portrait}{\@portraittrue}
\newif\ifinquestion

% Font size
\newcommand\@fontscaling{1}
\DeclareOptionX{fontsize-n}{\renewcommand\@fontscaling{1}}
\DeclareOptionX{fontsize-m}{\renewcommand\@fontscaling{1.1}}
\DeclareOptionX{fontsize-l}{\renewcommand\@fontscaling{1.2}}
\DeclareOptionX{fontsize-xl}{\renewcommand\@fontscaling{1.3}}
\DeclareOptionX{fontsize-xxl}{\renewcommand\@fontscaling{1.4}}

% Custom packages
\newcommand{\@custompackages}{}
\DeclareOptionX{custom-packages}[]{\renewcommand{\@custompackages}{#1}}

% Type of paper
\newif\if@coursework\@courseworkfalse
\DeclareOptionX{coursework}{\@courseworktrue}
\newif\if@exam\@examfalse
\DeclareOptionX{exam}{\@examtrue}
\newif\if@test\@testfalse
\DeclareOptionX{test}{\@testtrue}

% Level
\newcommand{\@level}{}
\newcommand{\@year}{}
\newif\if@ugone\@ugonefalse
\newif\if@ugtwo\@ugtwofalse
\newif\if@ugthree\@ugthreefalse
\newif\if@ugfour\@ugfourfalse
\newif\if@pg\@pgfalse
\DeclareOptionX{ug-1}{\@ugonetrue\renewcommand{\@year}{1}\renewcommand{\@level}{4}}
\DeclareOptionX{ug-2}{\@ugtwotrue\renewcommand{\@year}{2}\renewcommand{\@level}{5}}
\DeclareOptionX{ug-3}{\@ugthreetrue\renewcommand{\@year}{3}\renewcommand{\@level}{6}}
\DeclareOptionX{ug-4}{\@ugfourtrue\renewcommand{\@year}{4}\renewcommand{\@level}{7}}
\DeclareOptionX{pg}{\@pgtrue\renewcommand{\@year}{3/4}\renewcommand{\@level}{6/7}}

% Academic year
\newcommand{\@academicyear}{}
\DeclareOptionX{academic-year}[]{\renewcommand{\@academicyear}{20#1/\xinttheiexpr (#1 - 9) /: 10 \relax}}

% Additional material
\newcommand{\@additionalmaterial}{}
\DeclareOptionX{additional-material}[]{\renewcommand{\@additionalmaterial}{#1}}

% Calculators
\newcommand{\@calculators}{Calculators are not allowed.}
\DeclareOptionX{allow-calculators}{%
	\renewcommand{\@calculators}{%
		Candidates may only use calculators that are non-programmable,\\
		with no alphanumeric memory and not wireless enabled.%
	}%
}

% Credits
\newcommand{\@credits}{}
\DeclareOptionX{credits}[]{\renewcommand{\@credits}{#1}}

% Department
\newif\if@ismathematics\@ismathematicsfalse
\newif\if@isphysics\@isphysicsfalse
\newcommand{\@department}{}
\DeclareOptionX{department-mathematics}{\@ismathematicstrue\renewcommand{\@department}{Mathematics}}
\DeclareOptionX{department-physics}{\@isphysicstrue\renewcommand{\@department}{Physics}}

% Due date and tim
\newcommand{\@duedate}{}
\newcommand{\@duetime}{}
\DeclareOptionX{due-date}[]{\renewcommand{\@duedate}{#1}}
\DeclareOptionX{due-time}[]{\renewcommand{\@duetime}{#1}}

% External examiner
\newcommand{\@externalexaminer}{}
\DeclareOptionX{external-examiner}[]{\renewcommand{\@externalexaminer}{#1}}

% Handouts
\newcommand{\@handouts}{}
\DeclareOptionX{handouts}[]{\renewcommand{\@handouts}{#1}}

% Has parts (i.e. Part A, Part B, etc)
\newif\if@hasparts\@haspartsfalse
\DeclareOptionX{has-parts}{\@haspartstrue}

% Internal examiner
\newcommand{\@internalexaminer}{}
\DeclareOptionX{internal-examiner}[]{\renewcommand{\@internalexaminer}{#1}}

% Maximum mark
\newcommand{\@maximummark}{}
\DeclareOptionX{maximum-mark}[]{\renewcommand{\@maximummark}{#1}}

% Module code
\newcommand{\@modulecode}{}
\DeclareOptionX{module-code}[]{\renewcommand{\@modulecode}{#1}}

% Module title
\newcommand{\@moduletitle}{}
\DeclareOptionX{module-title}[]{\renewcommand{\@moduletitle}{#1}}

% Semester
\newcommand{\semester}{}
\DeclareOptionX{semester}[]{\renewcommand{\semester}{#1}}

% Show total marks of a question
\newif\if@hidemarksofquestion\@hidemarksofquestionfalse
\DeclareOptionX{hide-marks-of-question}{\@hidemarksofquestiontrue}

% Show URN field
\newif\if@showurn\@showurnfalse
\DeclareOptionX{show-urn}{\@showurntrue}

% Weighting
\newif\if@weighting\@weightingfalse
\DeclareOptionX{weighting}[]{
	\ifnum#1>20
		\@weightingtrue
	\fi
}

% default to blue answers
\newcommand\@showanswerscolour{blue}
\DeclareOptionX{show-answers-colour}[]{\renewcommand{\@showanswerscolour}{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RGI: Mark checking
%
\newif\if@markcheck\@markcheckfalse%
\DeclareOptionX{markcheck}{%
\@markchecktrue
}
\newif\if@markcheckeachpage\@markcheckeachpagefalse%
\DeclareOptionX{markcheck-eachpage}{%
\@markcheckeachpagetrue
}
\newif\if@markcheckeachline\@markcheckeachlinefalse%
\DeclareOptionX{markcheck-eachline}{%
\@markcheckeachlinetrue
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RGI: Answers
% default to no answers
\newif\if@showanswers\@showanswersfalse%
\newif\if@inanswer\@inanswerfalse%
% unless the option is given
\DeclareOptionX{show-answers}{%
\@showanswerstrue%
}

% Additional information on front page
\newcommand{\@rubric}{}
\newcommand{\rubric}[1]{%
	\if@ismathematics
		\renewcommand{\@rubric}{#1}%
	\fi
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Process options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareOptionX*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptionsX\relax

%
% Load packages
%

\RequirePackage{ifthen}
\RequirePackage{lastpage}
\usepackage{zref-totpages} % required to set footer on last page
%
\if@portrait
	\RequirePackage[
		text={
			6.5in,
			9.25in
		},
		centering,
		headheight=25pt
	]{geometry}
\else
	\RequirePackage[
		text={
			10in,
			5.8in
		},
		centering,
		headheight=25pt,
		landscape
	]{geometry}
	\RequirePackage{pdflscape}
\fi
%
\RequirePackage{cmbright}
%\RequirePackage{helvet}
%\renewcommand{\familydefault}{\sfdefault}
\RequirePackage{relsize}
\RequirePackage{fancyhdr}
\RequirePackage{textcomp}
\RequirePackage[T1]{fontenc}

% Load the custom packages
\renewcommand*{\do}[1]{\RequirePackage{#1}}
\dolistloop{\@custompackages}

%
% Date Settings
%

\RequirePackage[british]{babel}
\RequirePackage[useregional,showdow,calc]{datetime2}  

%
% Helpers
%

\newcommand{\@facultynotice}{%
	\textbf{Faculty of Engineering and Physical Sciences}%
}
\newcommand{\@copyrightnotice}{%
	\textcopyright\,\emph{This
		\if@coursework 
			coursework
		\fi
		\if@exam 
			examination
		\fi
		\if@test
			in-semester test 
		\fi
			paper is copyright of the University of Surrey and must not be reproduced, republished, or redistributed without written permission.%
	}%
}
\newcommand{\@marksnotice}{%
	Where appropriate, the mark carried by an individual part of a question\\ 
	is indicated in square brackets [~], and the maximum mark of this assessment is~\@maximummark.
}
\newcommand{\@onlinenotice}{%
	This is an open-book assessment. 
	\if@exam
		You have four hours to complete and submit the assessment; this assessment is designed to take two hours.
	\fi
	\if@test
		You have four hours to complete and submit the assessment; this assessment is designed to take one hour.
	\fi
	Submit your solutions \ifboolexpr{(bool {@exam} or bool {@test})}{as a single PDF to}{to} the assignment folder on the SurreyLearn module page. By doing so, you declare that your solutions are your own work. If you use any resources other than the lecture notes or handouts, you must acknowledge them. You are not allowed to collaborate on your work. Late submissions are not allowed, unless you have approved extenuating circumstances.%
}
\newcommand{\@universitynotice}{%
	\textbf{\Large\MakeUppercase{University of Surrey}\,$^{\mbox{\small\textcopyright}}$}%
}
\newcommand{\print}[2]{\ifthenelse{\equal{#1}{}}{}{#2}}
\newcommand\quellex[1]{{%
      \unskip\nobreak\hfil\penalty50%
      \hskip10pt\hbox{}\nobreak\hfil\textbf{#1}%
      \parfillskip=0pt\finalhyphendemerits=0\par%
}}

% RGI 
\newcommand\quelle[1]{{%
      \leavevmode\unskip\nobreak\hfil\penalty50%
      \hskip10pt\hbox{}\nobreak\hfil\textbf{#1}%
      \parfillskip=0pt\finalhyphendemerits=0\par%
}}

%
% Title page
%

% Mathematics; re-define certain options
\if@ismathematics
	% Duration
	\newcommand{\@duration}{%
		\textbf{%
			\if@exam 
				2 hours%
			\fi
			\if@test 
				50 minutes%
			\fi
		}%
	}
\fi

% Physics; re-define certain options
\if@isphysics
	% Duration
	\newcommand{\@duration}{%
		\textbf{%
			\if@exam 
				2 hours%
			\fi
			\if@test 
				50 minutes%
			\fi
		}%
	}
    % default additional material
	\providecommand{\@additionalmaterial}{%
		Physics Department Booklet of Formul{\ae} and Data%
	}
	\renewcommand{\@handouts}{1}
	\ifboolexpr{ 
		(bool {@exam} or bool {@test})
	}{
		% Rubric
		\renewcommand{\@rubric}{%
			Answer \textbf{\MakeUppercase{all}} questions in Part A.\\
			Answer \textbf{\MakeUppercase{two}} of \textbf{\MakeUppercase{three}} questions in Part B. If you attempt more than two questions in Part B, then only the best two solutions will be taken into account.\\
			Show full workings.
		}
	}{
		\renewcommand{\@rubric}{%
			Answer \textbf{ALL} questions.\\
			Show full workings.%
		}
	}
\fi

% Title page
\newcommand{\@frontpage}{
	\relscale{\@fontscaling}
	\begin{center}
  		\vspace*{1cm}
		
  		\@universitynotice
  		\vspace{1cm}
		
  		\@facultynotice
  		\vspace{5mm}
		
 		\textbf{Department of \@department}
 		\vspace{5mm}
		
 		\ifboolexpr{ 
			bool {@ugone} or bool {@ugtwo} or bool {@ugthree}
		}{
 			Undergraduate Programmes%
 		}{
			\ifboolexpr{
				bool {@ugfour}
			}{
				Undergraduate Programmes and Postgraduate Taught Programmes%
			}{
				\ifboolexpr{
					bool {@pg}
				}{
					Postgraduate Taught Programmes%
				}{}%
			}%
		} in~\@department
 		\vspace{1cm}
		
  		\textbf{%
  			\Large Module~\@modulecode%
  			\print{\@credits}{;~\@credits~Credits}%
  		}
  		\vspace{1cm}
		
  		\textbf{\Large\@moduletitle}
  		\vspace{1cm}
		
  		FHEQ Level~\@level~(Year~\@year)
  		\if@coursework
  			Coursework
  		\fi
		\if@exam 
			Examination
		\fi
		\if@test
			In-Semester Test
		\fi
		\vspace{1cm}
		
  		\if@portrait
  		\else
  			\pagebreak
  		\fi
		
		\ifboolexpr{ 
			(bool {@exam} or bool {@test}) and not bool {@online}
		}{
 			Time allowed:~\@duration\hfill\ifthenelse{\semester=0}{Late Summer Assessment}{Semester~\semester}~\@academicyear
 		}{
 			\print{\@duedate}{Due date: \DTMdate{\@duedate}~at~\@duetime}\hfill\ifthenelse{\semester=0}{Late Summer Assessment}{Semester~\semester}~\@academicyear
 		}
 		\vspace{1cm}
		
		\if@showurn
			\textbf{URN: ...........................................}
			\vspace{1cm}
		\fi
		
		\@rubric
		\vspace{5mm}
		
		\if@online
			\@onlinenotice
			\vspace{5mm}
		\fi
		
		\@marksnotice
		\vspace{5mm}
		
		\ifboolexpr{ 
			(bool {@exam} or bool {@test}) and not bool {@online}
		}{
			\@calculators
			\vspace{5mm}
		}
		
		\vfill
		\if@ismathematics
			\emph{Additional Material:}\\
			\print{\@additionalmaterial}{\@additionalmaterial,~}%
			\@handouts~\ifthenelse{\@handouts=1}{handout}{handouts}
		\fi
		\if@isphysics
			\emph{Additional Material:}\\
			\print{\@additionalmaterial}{\@additionalmaterial}
		\fi
		\vspace{5mm}

		\@copyrightnotice
  	\end{center}	
}


%
% Exam environment
%

\newif\ifexamstart
%
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%
\fancyhead[RO,RE]{}
\fancyhead[C]{}
\fancyhead[LO,LE]{\relscale{\@fontscaling}{\@modulecode/\pageref{LastPage}/\ifthenelse{\semester=0}{LSA}{SEMR\semester}~\@academicyear~(\@handouts~\ifthenelse{\@handouts=1}{handout}{handouts})}\markswarn{}}
\fancyfoot[C]{\vskip7pt\relscale{\@fontscaling}\thepage}
\fancyfoot[RO,RE]{%
	\relscale{\@fontscaling}
	\ifnum\value{page}=\ztotpages 
		\ifboolexpr{ 
			bool {@weighting} and (bool {@ugtwo} or bool {@ugthree} or bool {@ugfour} or bool {@pg})
		}{
			\vskip-24pt
			\begin{tabular}{r}
				\phantom{\MakeUppercase{External Examiner}:~\@externalexaminer}\\
				\textbf{\MakeUppercase{End of Paper\hspace{-.5cm}}}
			\end{tabular}
		}{%
			\textbf{\MakeUppercase{End of Paper\hspace{-.3cm}}}
		}
	\else
	\ifnum\thepage>1
        {
          \ifinquestion
          \textbf{\MakeUppercase{Question continues on next page\hspace{-.15cm}}}
          \else
          \textbf{\MakeUppercase{Questions continue on next page\hspace{-.15cm}}}
          \fi
        }
        \fi
	\fi
}
\fancyfoot[LO,LE]{%
  \relscale{\@fontscaling}
  \ifnum\value{page}=\ztotpages 
  % final page
		\ifboolexpr{ 
			bool {@weighting} and ( bool {@ugtwo} or bool {@ugthree} or bool {@ugfour} or bool {@pg} )
		}{
			\vskip-24pt
			\begin{tabular}{l}
				\MakeUppercase{External Examiner}:~\@externalexaminer\\
				\MakeUppercase{Internal Examiner}:~\@internalexaminer
			\end{tabular}
		}{
			\MakeUppercase{Internal Examiner}:~\@internalexaminer
		}
        \else
%        not the final page : do nothing
	\fi
}
%
\newenvironment{exam}{%
	\@frontpage
	\newpage
  	\par\vskip6pt\relax
}{%
  \debugging{}
  \notanswers{}
}

%
% Parts sections
%

\ifboolexpr{ 
	bool {@hasparts} 
}{
	\renewcommand{\section}{%
		\@startsection{section}{1}{\z@}{-3.5ex plus -1ex minus -.2ex}{2.3ex plus .2ex}{\large\bfseries\mathversion{bold}}%
	}
	\renewcommand{\theequation}{\Alph{section}.\arabic{equation}}
	\renewcommand{\thesection}{Part~\Alph{section}:}
	\ifboolexpr{ 
		bool {@isphysics}
	}{
		\newcommand{\exampart}[1][]{%
			\ifthenelse{\equal{#1}{}}{%
				\ifnum\value{section}=0
					\section{Answer ALL questions}
				\else
					\section{Answer TWO questions only}
				\fi
			}{%
				\ifnum\value{section}=0
					\section{Answer ALL questions}
				\else
					\section{Answer TWO questions only}
				\fi
			}%
		}
	}{
		\newcommand{\exampart}[1]{\section{#1}}
	}
}{
	\newcommand{\exampart}[1][]{%
		\ifthenelse{\equal{#1}{}}{}{}
	}
}
	
%
% Question environment
%

\setlength{\parindent}{0pt}
\RequirePackage{enumitem}
\setlist[enumerate,1]{label=(\alph*),ref=(\alph*)}
\setlist[enumerate,2]{label=(\roman*),ref=\theenumi(\roman*)}

\def\@begintheorem#1#2{\thmcontfont\trivlist\item[\hskip\labelsep\thmheadfont #1\ #2]}
\def\@opargbegintheorem#1#2#3{\thmcontfont\trivlist\item[\hskip\labelsep\thmheadfont #1\ #2\ (#3)]}
\let\thmheadfont\bf
\let\thmcontfont\upshape
\def\theoremheadfontis#1{\def\thmheadfont{#1}}
\def\theoremcontfontis#1{\def\thmcontfont{#1}}	
\newtheorem{qn}{Question}
\ifboolexpr{ 
	bool {@hasparts} 
}{
	\numberwithin{qn}{section}
	\renewcommand{\theqn}{\Alph{section}.\arabic{qn}}
}{}
\newenvironment{question}{\inquestiontrue%
	\qn\mbox{}%
	\vspace*{5pt}%
	\par\noindent\ignorespaces%
}{%
	\endqn%
	\ifboolexpr{ 
		bool {@hidemarksofquestion} 
	}{}{%
		\vspace*{-5pt}
		\marksofquestion{\theqn}% print total marks of the question
	}\inquestionfalse
}

%
% Debugging
%
\newcommand{\debugging}[1]{
  \newpage
  \noindent\fbox{%
    \begin{minipage}[t]{1\columnwidth }%
      \vspace{1cm}%
      Total marks in assessment: \marksofassessment
       
        \answer{\leavevmode\\ Total marks in answers: \marksofassessmentanswers }
     
      \vspace{1cm}%
      \markscheck{} %
    \end{minipage}%
}}

\ifboolexpr{ 
		bool {@markcheck} 
}{
% true : show debugging
}{
  %false : no debugging
  \renewcommand{\debugging}{{\markscheck{}}}
}

%
% Answers
%


% RGI: format answer here
%
\newcommand{\answerformat}[1]{
  {{\color{\@showanswerscolour}{\answernull #1}}}
}

% RGI: macro to set the @inanswer boolean
% inside an answer
\newcommand{\inanswer}[1]{%
\@inanswertrue%
{\answerformat{#1}}%
\@inanswerfalse%
}

% RGI: macro in which to wrap answer
\newcommand{\answer}[1]{\inanswer{#1}}

% now, how to format answers?
\newsavebox{\xxxtrashbox}
\ifboolexpr{ 
		bool {@showanswers} 
}{
  % true : show answers
  \newcommand{\answernull}{}
}{
  %false : no answers
  \newcommand{\answernull}{\nullfont\let\par\relax}
  \renewcommand{\answer}[1]{\sbox{\xxxtrashbox}{\nullfont\inanswer{\begin{minipage}{\linewidth}{#1}\end{minipage}}}}
}

%
% Marks
%

\usepackage{xparse}

\ExplSyntaxOn
	% Initiate global containers
	\fp_new:N \g_marks_of_question_fp
	\fp_new:N \g_marks_of_answer_fp
	\fp_new:N \g_marks_of_assessment_fp
	\fp_new:N \g_marks_of_assessment_answers_fp
	% Set global containers
	\fp_gset:Nn \g_marks_of_question_fp {0}
	\fp_gset:Nn \g_marks_of_answer_fp {0}
	\fp_gset:Nn \g_marks_of_assessment_fp {0}
	\fp_gset:Nn \g_marks_of_assessment_answers_fp {0}
    
	% Points
	\NewDocumentCommand{\points}{m}
 	{
      \if@inanswer
      {
            % inside an answer
            \fp_gadd:Nn \g_marks_of_answer_fp { #1 } % add to the global container (marks of answer)
  			\fp_gadd:Nn \g_marks_of_assessment_answers_fp { #1 } % add to the global container (marks of assignment)
      }
      \else
      {
            % not inside an answer
		    \fp_gadd:Nn \g_marks_of_question_fp { #1 } % add to the global container (marks of question)
  			\fp_gadd:Nn \g_marks_of_assessment_fp { #1 } % add to the global container (marks of assignment)
      }
      \fi
      
      
      % print the marks
	  \if@inanswer
      {
        \fp_compare:nTF { #1 = 1 } 
		   {
 			 \quelle{\answernull{}[#1~mark]}
		   }
		   {
 			 \quelle{\answernull{}[#1~marks]}
		   }
	  }
      \else
      {
        \fp_compare:nTF { #1 = 1 } 
		   {
 			 \quelle{[#1~mark]}
		   }
		   {
 			 \quelle{[#1~marks]}
		   }
	  }
      \fi
    }
      
	  % Points per questions
	  \DeclareExpandableDocumentCommand{\marksofquestion}{m}
      {
		  % print marks of the question
	      \fp_compare:nTF { \g_marks_of_question_fp = 1 } 
			 {
 			   \quelle{[Total~for~Question~#1:~\fp_eval:n { \g_marks_of_question_fp }~mark]}
			 }
			 {
 			   \quelle{[Total~for~Question~#1:~\fp_eval:n { \g_marks_of_question_fp }~marks]}
			 }
                
             % print marks of the answer

               \answer{
                 \fp_compare:nTF { \g_marks_of_answer_fp = 1 }
				{
 				  \answerformat{\quelle{\answernull{}[Total~for~Answer~#1:~\fp_eval:n { \g_marks_of_answer_fp }~mark]}}
				}
				{
 				  \answerformat{\quelle{\answernull{}[Total~for~Answer~#1:~\fp_eval:n { \g_marks_of_answer_fp }~marks]}}
				}
               }
             

           % reset counters
    	   \fp_gset:Nn \g_marks_of_question_fp { 0 }
           \fp_gset:Nn \g_marks_of_answer_fp { 0 }
 		}
	% Total points
	\DeclareExpandableDocumentCommand{\marksofassessment}{}
 		{
  			\fp_eval:n { \g_marks_of_assessment_fp } % print marks of the assessment
 		}

        
		\DeclareExpandableDocumentCommand{\marksofassessmentanswers}{}
 		{
  			\fp_eval:n { \g_marks_of_assessment_answers_fp } % print marks of the assessment
 		}

        \DeclareExpandableDocumentCommand{\markscheck}{}
        {

          \fp_compare:nTF { \g_marks_of_assessment_fp = \g_marks_of_assessment_answers_fp }
             {}{\textbf{\\ \color{red}{WARNING:~marks~of~questions~and~answers~do~not~match!}}}
        }

        \if@markcheckeachpage
 		\DeclareExpandableDocumentCommand{\markswarn}{}
        {
          \fp_compare:nTF { \g_marks_of_assessment_answers_fp = 0 }
             {
               % output nothing if no answers marks are stored
             }
             {
          \fp_compare:nTF { \g_marks_of_assessment_fp = \g_marks_of_assessment_answers_fp }
             {}{
               % mark count of answers != mark count of questions : warn
               \inconsistentmarks{}
             }
             }
        }
        \else
        \DeclareExpandableDocumentCommand{\markswarn}{}{}
        \fi
        
        
\ExplSyntaxOff

\newcommand{\inconsistentmarks}[1]{
\textbf{
  {\color{red}{~Q~and~A~marks~are~inconsistent}}
}        
}
%
% Extra commands
%

% Bold dot over letter
\renewcommand{\dot}[1]{
	\overset{
		\begin{tikzpicture}
			\fill (0,0) circle[radius=0.7pt];
		\end{tikzpicture}
	}{#1}
}

% Bold double dot over letter
\renewcommand{\ddot}[1]{
	\overset{
		\begin{tikzpicture}
			\fill (-0.04,0) circle[radius=0.7pt];
			\fill (0.04,0) circle[radius=0.7pt];
		\end{tikzpicture}
	}{#1}
}

% RGI: force document environment to use exam
% environment, saves you having to type it
\AtBeginDocument{\begin{exam}}
\AtEndDocument{\end{exam}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% These are the question categories. Please use these to describe the type of 
% question. These descriptors should be used also for each part of a question:
% A: Accessible core knowledge, recall, concepts, elementary theory and seen problems requiring minimal ability.
% B: Bread-and-butter unseen problems, algebra, derivations, explanations and meanings requiring clear ability.
% C: Challenging, e.g. advanced concepts, complex derivations, deeper knowledge/insight and advanced problems.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\QA}{ A: Accessible (bookwork / core) $\mid$}
\newcommand{\QB}{ B: Bread-and-butter (unseen) $\mid$}
\newcommand{\QC}{ C: Challenging $\mid$}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Macros for LyX integration %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% macro to disable things when not using the answers branch


\newcommand\notanswers{
  % not in answers branch : disable answers and mark checking macros
  \@markcheckeachpagefalse
  \@markcheckfalse
  \@hidemarksofquestionfalse
  \@showanswersfalse
  \@inanswerfalse
  \renewcommand{\markswarn}[1]{}
  \renewcommand{\markscheck}[1]{}
  \renewcommand{\marksofquestion}[1]{}
  \renewcommand{\debugging}[1]{}
  \renewcommand{\inconsistentmarks}[1]{}
  \renewcommand{\answer}[1]{}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% extra formatting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\raggedbottom

% counters within enumerated lists
\usepackage{chngcntr}
\counterwithin*{equation}{enumi}

% numbering
\usepackage{lineno}
\linenumbers
\ExplSyntaxOn
\renewcommand\thelinenumber{
  \if@markcheckeachline
  \fp_compare:nTF { \g_marks_of_assessment_answers_fp = 0 }
     {
       % no marks yet
     }
     {
       \fp_compare:nTF { \g_marks_of_assessment_fp = \g_marks_of_assessment_answers_fp }
          {
            % marks of Q and A equal : ok!
            \color{black}{ Q \fp_eval:n { \g_marks_of_assessment_fp }, A \fp_eval:n { \g_marks_of_assessment_answers_fp }}
          }{
            % marks of Q and A unequal: bad!
            \color{red}{ Q \fp_eval:n { \g_marks_of_assessment_fp }, A \fp_eval:n { \g_marks_of_assessment_answers_fp }}
          }
     }
     \else
         {}
         \fi
}
\ExplSyntaxOff
